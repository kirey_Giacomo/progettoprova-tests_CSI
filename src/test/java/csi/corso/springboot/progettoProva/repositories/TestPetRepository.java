package csi.corso.springboot.progettoProva.repositories;

import csi.corso.springboot.progettoProva.entities.CategoryEntity;
import csi.corso.springboot.progettoProva.entities.PetEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TestPetRepository {

    @Mock
    PetRepository petRepository;

    private PetEntity initPet(){
        // this function returns a PetEntity with id and name
    	CategoryEntity cat = new CategoryEntity();
    	cat.setId(1L);
    	cat.setName("CategoryName");
    	PetEntity ent = new PetEntity();
    	ent.setId(1L);
    	ent.setName("PetName");
    	ent.setCategory(cat);    	
        return ent;
    }

    @Test
    void petRepositorySave() {
        PetEntity pet = initPet();
        when(petRepository.save(any())).thenReturn(pet);
        PetEntity saved = petRepository.save(pet);
        assert(pet != null);
        assert(saved != null);
        Assertions.assertEquals(saved.getId(), pet.getId());
    }

    @Test
    void petRepositoryFindAll(){
        PetEntity pet = initPet();
        assert(pet != null);
        when(petRepository.save(any())).thenReturn(pet);
        PetEntity saved = petRepository.save(pet);
        assert(saved != null);
        List<PetEntity> all = new ArrayList<>();
        all.add(pet);
        all.add(saved);
        Assert.isTrue(petRepository.findAll().size() == 0, "Size Error");
        when(petRepository.findAll()).thenReturn(all);
        List<PetEntity> afterSearch = petRepository.findAll();
        Assert.notEmpty(afterSearch, "Empty List");
    }

    @Test
    void petRepositoryFindById(){
    	PetEntity pet = initPet();
    	Optional<PetEntity> optPet = Optional.of(pet);
    	assert(pet != null);
    	when(petRepository.save(any())).thenReturn(pet);
        when(petRepository.findById(any())).thenReturn(optPet); //from org.mockito.Mockito.when to mock the petRepository.save() call
        PetEntity saved = petRepository.save(pet);
        assert(saved != null);
        // use when().thenReturn() from org.mockito.Mockito.when to mock the petRepository.findById() call
        Optional<PetEntity> found = petRepository.findById(saved.getId());
        assert(found != null);
        found.ifPresent(pez -> {
            Assertions.assertEquals(pet.getId(), pez.getId());
        });
    }
}
