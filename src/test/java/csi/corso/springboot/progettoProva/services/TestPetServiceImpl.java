package csi.corso.springboot.progettoProva.services;

import csi.corso.springboot.progettoProva.api.model.generated.Pet;
import csi.corso.springboot.progettoProva.entities.PetEntity;
import csi.corso.springboot.progettoProva.repositories.PetRepository;
import org.apache.http.auth.BasicUserPrincipal;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.validation.Validator;
import javax.validation.metadata.ConstraintDescriptor;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
 class TestPetServiceImpl {

	
	@InjectMocks
	PetServiceImpl petServiceImpl;
	
	@Mock
	PetRepository petRepository;
	
	@Mock 
	Validator validator;
	
	@Mock
	SecurityContext securityContext;

//	@Test
	void savePetTest() {
		PetEntity pe = new PetEntity();
		pe.setId(1L);
		
		
		when(petRepository.save(any())).thenReturn(pe);
		Pet p = new Pet();
		petServiceImpl.savePet(p);
		assertEquals(pe.getId(),p.getId());
		
		Set<ConstraintViolation<Pet>> cvp = new HashSet<>();
		cvp.add(new ConstraintViolation<Pet> () {

			@Override
			public String getMessage() {
				// TODO Auto-generated method stub
				return "Mocked Violation";
			}

			@Override
			public String getMessageTemplate() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Pet getRootBean() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Class<Pet> getRootBeanClass() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getLeafBean() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object[] getExecutableParameters() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getExecutableReturnValue() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Path getPropertyPath() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getInvalidValue() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public ConstraintDescriptor<?> getConstraintDescriptor() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public <U> U unwrap(Class<U> type) {
				// TODO Auto-generated method stub
				return null;
			}
			
			
		} );
		when(validator.validate(p)).thenReturn(cvp);
		;
		assertThrows(ConstraintViolationException.class, () ->petServiceImpl.savePet(p)); 
	}
	
//	@Test
	void asyncFunctionTest() {
		Authentication auth = new UsernamePasswordAuthenticationToken(new BasicUserPrincipal("test"),"password");
		try (MockedStatic<SecurityContextHolder> sch = mockStatic(SecurityContextHolder.class)){
			sch.when(SecurityContextHolder::getContext).thenReturn(securityContext);
			when(securityContext.getAuthentication()).thenReturn(auth);
			assertDoesNotThrow(()->petServiceImpl.asyncFunction());
			
		}
	}

}
