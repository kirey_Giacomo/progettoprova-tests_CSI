package csi.corso.springboot.progettoProva.junit;

import csi.corso.springboot.progettoProva.api.model.generated.Category;
import csi.corso.springboot.progettoProva.api.model.generated.Pet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@DataJpaTest
class ProgettoProvaApplicationTests {

    @Test
    void testPetCreation() {
        Assertions.assertNotNull(initPet());
    }

    @Test
    void testCategoryCreation() {
        Assertions.assertNotNull(initCategory());
    }

    @Test
    void testPetWithCategory() {
        Pet pet = initPet(); // init a Pet
        pet.setCategory(initCategory()); 
        // add a category to pet
        assert(pet != null);
        Assertions.assertNotNull(pet.getCategory());
    }

    @Test
    void testPetWithName() {
        Pet pet = getNamedPet(); // init a named pet
        assert(pet != null);
        Assertions.assertEquals("PetName", pet.getName());
    }

    @Test
    void testCategoryWithName() {
        Category category = getNamedCategory(); // init a named category
        assert(category != null);
        Assertions.assertEquals("CategoryName", category.getName());
    }

    @Test
    void testNamedPetWithCategory() {
        Pet pet = getNamedPetWithNamedCategory(); // init a named pet
        Assertions.assertNotNull(pet);
        Assertions.assertEquals("PetName", pet.getName());
        Category category = pet.getCategory(); // init a named category
        Assertions.assertNotNull(category);
        // add a named category to named pet
        Assertions.assertNotNull(pet.getCategory());
        Assertions.assertEquals("CategoryName", pet.getCategory().getName());
    }

    @Test
    void testFailureNamedPet() {
        Pet pet = initPet(); // init a named pet
        assert(pet != null);
        Assertions.assertNotEquals("testPet", pet.getName());
    }

    @Test
    void testFailureNamedPetWithCategory() {
        Pet pet = getNamedPetWithNamedCategory(); // init a named pet
        Assertions.assertNotNull(pet);
        Assertions.assertNotNull(pet.getCategory());
        Assertions.assertNotEquals("testPetCategory", pet.getCategory().getName());
    }

    private Pet initPet() {
        // this function creates a new Pet when it's called
        return new Pet();
    }

    private Pet getNamedPet() {
        // this function calls initPet() and sets a name to the pet
       Pet pet = new Pet();
       pet.setName("PetName");
       return pet;
    }

    private Category initCategory() {
        // this function creates a new Category when it's called
        return new Category();
    }

    private Category getNamedCategory() {
        Category category = initCategory();
        category.setName("CategoryName");
        return category;
    }
    
    private Pet getNamedPetWithNamedCategory() {
    	Pet pet = getNamedPet();
    	pet.setCategory(getNamedCategory());
    	return pet;
    	
    }

}
