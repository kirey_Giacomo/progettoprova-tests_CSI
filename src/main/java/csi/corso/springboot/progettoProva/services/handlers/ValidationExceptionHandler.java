package csi.corso.springboot.progettoProva.services.handlers;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Provider
@Component
public class ValidationExceptionHandler implements ExceptionMapper<ConstraintViolationException> {

	 private static final Logger log = LoggerFactory.getLogger(ValidationExceptionHandler.class);
	
	@Override
	public Response toResponse(ConstraintViolationException e) {
		 log.error("Exiting with error code: {}, message {}", HttpStatus.SC_BAD_REQUEST,e.getMessage());
	     return Response.status(HttpStatus.SC_BAD_REQUEST).entity(e.getMessage()).build();
	}
}
