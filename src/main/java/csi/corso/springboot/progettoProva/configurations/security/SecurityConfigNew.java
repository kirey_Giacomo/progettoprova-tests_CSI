
package csi.corso.springboot.progettoProva.configurations.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfigNew {
	/*	
	
	@Bean
    public InMemoryUserDetailsManager inMemoryUserDetailsService() {
    	PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    	UserDetails user = User.withUsername("user")
                .password(encoder.encode("password"))
                .roles("USER")
                .build();
    	UserDetails admin = User.withUsername("admin")
            .password(encoder.encode("password"))
            .roles("USER","ADMIN")
            .build();
        return new InMemoryUserDetailsManager(user,admin);
    }
	*/	

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth,DataSource dataSource)
	  throws Exception {
    	PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    	UserDetails user = User.withUsername("user")
                .password(encoder.encode("password"))
                .roles("USER")
                .build();
    	UserDetails admin = User.withUsername("admin")
            .password(encoder.encode("password"))
            .roles("USER","ADMIN")
            .build();
		auth.jdbcAuthentication()
	      .dataSource(dataSource)
	      .withDefaultSchema().withUser(User.withUsername("user2")
	    	        .password(encoder.encode("password"))
	    	        .roles("USER")).and().inMemoryAuthentication().withUser(admin).withUser(user);
		
	}

// */	

	/*CUSTOM PROVIDER 
	*
	*
    @Autowired
    private FromLibraryAuthenticationProvider authProvider;

    @Bean
    public AuthenticationManager authManager(HttpSecurity http) throws Exception {
        AuthenticationManagerBuilder authenticationManagerBuilder = 
            http.getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder.authenticationProvider(authProvider);
        return authenticationManagerBuilder.build();
    }
*/	
	
	
	
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.headers().frameOptions().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().csrf().disable()
        .authorizeRequests()
                .antMatchers("/h2/**").permitAll().and().authorizeRequests()
                .antMatchers("/api/**").authenticated().and().httpBasic();

	    return http.build();
	}
	
}
