package csi.corso.springboot.progettoProva.configurations;

import java.time.LocalTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.task.DelegatingSecurityContextAsyncTaskExecutor;

import csi.corso.springboot.progettoProva.beans.PrototypeBean;
import csi.corso.springboot.progettoProva.beans.SingletonBean;

@Configuration
@EnableAsync
public class AppConfigurator {

	
	Logger logger = LoggerFactory.getLogger(AppConfigurator.class);
	

	@Value("${spring.application.name:provaprova}")
	private String myConfigVariab;
	
	@Value("${resteasy.jaxrs.app.classes}")
	private String resteasyconf;
	
    @Bean
    @Scope(org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    //@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)  // 3
    public PrototypeBean prototypeBean(Environment lt) {
    	 PrototypeBean pb = new PrototypeBean();
    	 if("ProgettoProvaDEV".equals(myConfigVariab)) {
    		 pb.setMyTime(LocalTime.now());
    	 }
    	 return pb;
    }
    

    //0 //2

    @Bean 
    public SingletonBean singletonBean() {
    	logger.info("valore variabile: {}",myConfigVariab);
    	logger.info("resteasyConf: {}",resteasyconf);
    	return new SingletonBean();
    }


    @Bean
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(100);
        executor.setQueueCapacity(50);
        executor.setThreadNamePrefix("Async_Thread-");
        return executor;
    }

    @Bean
    public DelegatingSecurityContextAsyncTaskExecutor taskExecutor(ThreadPoolTaskExecutor delegate) {
        return new DelegatingSecurityContextAsyncTaskExecutor(delegate);
    }

   
    /* FUNCTIONAL
     *
     *
    @Bean
    public Function<String, PrototypeBean> beanFactory() {
        return name -> prototypeBeanWithParam(name);
    } 

    @Bean
    @Scope(value = "prototype")
    public PrototypeBean prototypeBeanWithParam(String name) {
       return new PrototypeBean(name);
    }
    
    @Bean
    public SingletonFunctionBean singletonFunctionBean() {
        return new SingletonFunctionBean();
    }
    * 
    * 

    
    @Bean
    SpringDocConfiguration springDocConfiguration(){
       return new SpringDocConfiguration();
    }
    @Bean
    SpringDocConfigProperties springDocConfigProperties() {
       return new SpringDocConfigProperties();
    }

    @Bean
    ObjectMapperProvider objectMapperProvider(SpringDocConfigProperties springDocConfigProperties){
        return new ObjectMapperProvider(springDocConfigProperties);
    }
	    */
}
