
package csi.corso.springboot.progettoProva.api.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Objects;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Path("openapi.yaml")
@Component
public class StaticResourcesResource {

	  @Autowired ServletContext context;
	  @Value("${custom.current.yamlfile:}") String pathToYaml;

	  @GET
	  public Response staticResources() {

	    InputStream resource = null;
		try {
			resource = new FileInputStream(pathToYaml);
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
	    return Objects.isNull(resource)
	        ? Response.status(HttpStatus.SC_NOT_FOUND).build()
	        : Response.ok().entity(resource).build();
	  }
}