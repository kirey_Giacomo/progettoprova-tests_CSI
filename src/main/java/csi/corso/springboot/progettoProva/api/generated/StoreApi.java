package csi.corso.springboot.progettoProva.api.generated;

import csi.corso.springboot.progettoProva.api.model.generated.*;


import java.util.Map;
import csi.corso.springboot.progettoProva.api.model.generated.Order;

import java.util.List;
import java.util.Map;

import java.io.InputStream;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.HttpHeaders;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.validation.constraints.*;
@Path("/store")


public interface StoreApi  {
   
    @DELETE
    @Path("/order/{orderId}")
    
    
    Response deleteOrder( @PathParam("orderId") Long orderId,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @GET
    @Path("/inventory")
    
    @Produces({ "application/json" })
    Response getInventory(@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @GET
    @Path("/order/{orderId}")
    
    @Produces({ "application/json" })
    Response getOrderById( @PathParam("orderId") Long orderId,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @POST
    @Path("/order")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    Response placeOrder( Order body,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

}
