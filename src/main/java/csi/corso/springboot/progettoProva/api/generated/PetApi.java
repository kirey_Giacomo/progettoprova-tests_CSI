package csi.corso.springboot.progettoProva.api.generated;

import csi.corso.springboot.progettoProva.api.model.generated.*;


import csi.corso.springboot.progettoProva.api.model.generated.ModelApiResponse;
import csi.corso.springboot.progettoProva.api.model.generated.Pet;

import java.util.List;
import java.util.Map;

import java.io.InputStream;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.HttpHeaders;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.validation.constraints.*;
@Path("/pet")


public interface PetApi  {
   
    @POST
    
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    Response addPet( Pet body,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @DELETE
    @Path("/{petId}")
    
    
    Response deletePet( @PathParam("petId") Long petId, @HeaderParam("api_key") String apiKey,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @GET
    @Path("/findByStatus")
    
    @Produces({ "application/json" })
    Response findPetsByStatus( @QueryParam("status") String status,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @GET
    @Path("/findByTags")
    
    @Produces({ "application/json" })
    Response findPetsByTags( @QueryParam("tags") List<String> tags,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @GET
    @Path("/pets")
    
    @Produces({ "application/json" })
    Response getAllPets(@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @GET
    @Path("/{petId}")
    
    @Produces({ "application/json" })
    Response getPetById( @PathParam("petId") Long petId,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @PUT
    
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    Response updatePet( Pet body,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @POST
    @Path("/{petId}")
    
    
    Response updatePetWithForm( @PathParam("petId") Long petId, @QueryParam("name") String name, @QueryParam("status") String status,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @POST
    @Path("/{petId}/uploadImage")
    @Consumes({ "application/octet-stream" })
    @Produces({ "application/json" })
    Response uploadFile( @PathParam("petId") Long petId, Object body, @QueryParam("additionalMetadata") String additionalMetadata,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

}
