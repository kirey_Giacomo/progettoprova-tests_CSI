package csi.corso.springboot.progettoProva.api.model.mapper;

import java.util.function.Function;

public class EntityMapper  {

	private EntityMapper() {
		throw new IllegalStateException("Utility class");

	}
	
	public static <E,D> E map(D input,Function<D,E> func) {
		return func.apply(input);
	}

}
