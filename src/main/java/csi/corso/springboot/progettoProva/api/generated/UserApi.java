package csi.corso.springboot.progettoProva.api.generated;

import csi.corso.springboot.progettoProva.api.model.generated.*;


import csi.corso.springboot.progettoProva.api.model.generated.User;

import java.util.List;
import java.util.Map;

import java.io.InputStream;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.HttpHeaders;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.validation.constraints.*;
@Path("/user")


public interface UserApi  {
   
    @POST
    
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    Response createUser( User body,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @POST
    @Path("/createWithList")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    Response createUsersWithListInput( List<User> body,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @DELETE
    @Path("/{username}")
    
    
    Response deleteUser( @PathParam("username") String username,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @GET
    @Path("/{username}")
    
    @Produces({ "application/json" })
    Response getUserByName( @PathParam("username") String username,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @GET
    @Path("/login")
    
    @Produces({ "application/json" })
    Response loginUser( @QueryParam("username") String username, @QueryParam("password") String password,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @GET
    @Path("/logout")
    
    
    Response logoutUser(@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

    @PUT
    @Path("/{username}")
    @Consumes({ "application/json" })
    
    Response updateUser( @PathParam("username") String username, User body,@Context SecurityContext securityContext, @Context HttpHeaders httpHeaders , @Context HttpServletRequest httpRequest );

}
