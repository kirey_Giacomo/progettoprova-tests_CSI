package csi.corso.springboot.progettoProva.api.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import csi.corso.springboot.progettoProva.api.generated.PetApi;
import csi.corso.springboot.progettoProva.api.model.generated.Pet;
import csi.corso.springboot.progettoProva.configurations.security.custom_annotations.IsAdmin;
import csi.corso.springboot.progettoProva.configurations.security.custom_annotations.IsUser;
import csi.corso.springboot.progettoProva.services.PetServiceImpl;


@Component
public class PetApiImpl implements PetApi {

	private static final Logger log = LoggerFactory.getLogger(PetApiImpl.class); 
	@Autowired
	private PetServiceImpl psi;

	@Override
	@IsAdmin
	public Response addPet(Pet body, SecurityContext securityContext, HttpHeaders httpHeaders,
			HttpServletRequest httpRequest) {
		psi.asyncFunction();
		return Response.ok(psi.savePet(body)).build();
	}

	@Override
	public Response deletePet(Long petId, String apiKey, SecurityContext securityContext, HttpHeaders httpHeaders,
			HttpServletRequest httpRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response findPetsByStatus(String status, SecurityContext securityContext, HttpHeaders httpHeaders,
			HttpServletRequest httpRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response findPetsByTags(List<String> tags, SecurityContext securityContext, HttpHeaders httpHeaders,
			HttpServletRequest httpRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response getAllPets(SecurityContext securityContext, HttpHeaders httpHeaders, HttpServletRequest httpRequest) {
		return Response.ok(psi.getAllPets()).build();
	}

	@Override
	//@PreAuthorize("hasAuthority('ROLE_USER')")
	@IsUser
	public Response getPetById(Long petId, SecurityContext securityContext, HttpHeaders httpHeaders,
			HttpServletRequest httpRequest) {
		psi.asyncFunction();
		log.info("get pet by id engaged");
		return Response.ok(psi.getById(petId)).build();
	}

	@Override
	public Response updatePet(Pet body, SecurityContext securityContext, HttpHeaders httpHeaders,
			HttpServletRequest httpRequest) {
		// TODO Auto-generated method stub
		return Response.ok(psi.savePet(body)).build();
	}

	@Override
	public Response updatePetWithForm(Long petId, String name, String status, SecurityContext securityContext,
			HttpHeaders httpHeaders, HttpServletRequest httpRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response uploadFile(Long petId, Object body, String additionalMetadata, SecurityContext securityContext,
			HttpHeaders httpHeaders, HttpServletRequest httpRequest) {
		// TODO Auto-generated method stub
		return null;
	}



}
