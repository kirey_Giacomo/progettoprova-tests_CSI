package csi.corso.springboot.progettoProva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgettoProvaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgettoProvaApplication.class, args);
	}

}
