FROM openjdk:11
EXPOSE 8080
ADD target/spring-to-docker.jar spring-to-docker.jar
ENV spring_profiles_active=dev
ENV variable=nomeDev
ENTRYPOINT ["java", "-jar", "spring-to-docker.jar"]